import csv


def get_erp_plan_from_csv(csv_file):
    with open(csv_file, 'r') as input_file:
        data = csv.DictReader(input_file)
        report = [{
            'identity': '{}_{}'.format(row['CODE'], row['DATE_TO']),
            'transitionIdentity': row['CODE'],
            'date': row['DATE_TO'].replace('.', '-'),
            'quantityPlanERP': int(row['AMOUNT']),
        } for row in data]
    return report

