from datetime import datetime, date, timedelta
from functools import partialmethod
from collections import defaultdict
from json import JSONDecodeError
from math import floor
from urllib.parse import urljoin

from requests import Session

from utils.list_to_dict import list_to_dict
from .base import Base

__all__ = [
    'IAImportExport',
]

_DATETIME_SIMPLE_FORMAT = '%Y-%m-%dT%H:%M:%S'


class IAImportExport(Base):

    def __init__(self, login, password, base_url,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._base_url = base_url
        self._login = login
        self._password = password

        self._session = Session()
        self._session.verify = False

        self.cache = {}

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._session.close()

    def _make_url(self, uri):
        return urljoin(self._base_url, uri)

    @staticmethod
    def _make_entity_name(filename, timestamp=datetime.now()):
        return '({}) {}'.format(
                    timestamp.strftime(_DATETIME_SIMPLE_FORMAT),
                    filename
                )

    def _get_from_rest_collection(self, table):
        if table not in self.cache:
            self.cache[table] = self._perform_get(
                'rest/collection/{}'.format(table)
            )[table]
        return self.cache[table]

    def _get_main_session(self):
        return self._perform_get('action/primary_simulation_session')['data']

    def _perform_json_request(self, http_method, uri, **kwargs):
        url = self._make_url(uri)
        logger = self._logger

        logger.info('Выполнение {} запроса '
                    'по ссылке {!r}.'.format(http_method, url))

        logger.debug('Отправляемые данные: {!r}.'.format(kwargs))

        response = self._session.request(http_method,
                                         url=url,
                                         **kwargs)
        try:
            response_json = response.json()
        except JSONDecodeError:
            logger.error('Получен ответ на {} запрос по ссылке {!r}: '
                         '{!r}'.format(http_method, url, response))
            raise JSONDecodeError

        logger.debug('Получен ответ на {} запрос по ссылке {!r}: '
                     '{!r}'.format(http_method, url, response_json))
        return response_json

    _perform_get = partialmethod(_perform_json_request, 'GET')

    def _perform_post(self, uri, data):
        return self._perform_json_request('POST', uri, json=data)

    def _perform_put(self, uri, data):
        return self._perform_json_request('PUT', uri, json=data)

    def _perform_action(self, uri_part, **data):
        return self._perform_post(
            '/action/{}'.format(uri_part),
            data=data
        )

    def _perform_login(self):
        return self._perform_action(
            'login',
            data={
                    'login': self._login,
                    'password': self._password
                },
            action='login'
        )['data']

    def export_entities(self):
        self._perform_login()
        entities = self._get_from_rest_collection('entity')

        report = []
        for row in entities:
            report.append({
                'identity': row['identity'],
                'name': row['name'],
                'vendorCode': row['identity']
            })

        return report

    def export_departments(self):
        self._perform_login()
        departments = self._get_from_rest_collection('department')

        report = []
        for row in departments:
            report.append({
                'identity': row['identity'],
                'name': row['name'],
            })

        return report

    def export_ca_equipment(self):
        self._perform_login()
        equipment = self._get_from_rest_collection('equipment')

        report = []
        for row in equipment:
            report.append({
                'identity': row['identity'],
                'className': row['name'],
            })

        return report

    def export_ca_spec(self):
        self._perform_login()
        entities_dict = list_to_dict(self._get_from_rest_collection('entity'))
        specification = self._get_from_rest_collection('specification_item')

        spec = defaultdict(list)
        for row in specification:
            spec[entities_dict[row['parent_id']]['identity']].append({
                'assemblyElementIdentity': entities_dict[
                    row['parent_id']
                ]['identity'],
                'quantityAssemblyElement': row['amount']
            })

        report = []
        for parent in spec:
            report.append({
                'identity': parent,
                'parentAssemblyElementIdentity': parent,
                'items': spec[parent]
            })

        return report

    def export_routes(self):
        self._perform_login()
        entity_dict = list_to_dict(
            self._get_from_rest_collection('entity')
        )
        routes = self._get_from_rest_collection('entity_route')

        report = []
        for row in routes:
            entity_id = row['entity_id']
            if row['alternate']:
                alternative = 1
            else:
                alternative = 0

            report.append({
                'ROUTE_ID': row['identity'],
                'CODE': entity_dict[entity_id]['identity'],
                'ALTERNATIVE': alternative,
            })

        return report

    def export_ca_routes(self):
        self._perform_login()
        entity_dict = list_to_dict(
            self._get_from_rest_collection('entity')
        )
        routes = self._get_from_rest_collection('entity_route')

        report = [{
            'identity': row['identity'],
            'assemblyElementIdentity': entity_dict[row['entity_id']]['identity'],
            'name': row['identity'],
        } for row in routes]

        return report

    def export_operations(self):

        self._perform_login()
        entity_routes_dict = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )
        operations = self._get_from_rest_collection('operation')
        departments_dict = list_to_dict(
            self._get_from_rest_collection('department')
        )
        equipments_class_dict = list_to_dict(
            self._get_from_rest_collection('equipment')
        )

        return [
            {
                'ROUTE_ID': entity_routes_dict[
                    row['entity_route_id']
                ]['identity'],
                'ID': row['identity'],
                'DEPT_ID': departments_dict[row['department_id']]['identity'],
                'EQUIPMENT_ID': equipments_class_dict[
                    row['equipment_class_id']
                ]['identity'],
                'NAME': "###",
                'NOP': row['nop'],
                'T_SHT': row['prod_time'],
                'T_PZ': row['prep_time'],
                'T_NAL': row['setup_time'],
            } for row in operations
        ]

    def export_phases(self):

        self._perform_login()
        entity_dict = list_to_dict(
            self._get_from_rest_collection('entity')
        )
        entity_routes_dict = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )
        operations = sorted(
            self._get_from_rest_collection('operation'),
            key=lambda k: k['nop']
        )
        departments_dict = list_to_dict(
            self._get_from_rest_collection('department')
        )

        prev_department = {}
        dept_route = {}
        for row in operations:
            entity_route_id = row['entity_route_id']
            if entity_route_id not in dept_route:
                dept_route[entity_route_id] = [None]
                prev_department[entity_route_id] = None
            dept = departments_dict[row['department_id']]['identity']
            if prev_department[entity_route_id] != dept:
                dept_route[entity_route_id].append(dept)
                prev_department[entity_route_id] = dept
        for route in dept_route:
            dept_route[route].append(None)
        report = []
        prev_department = {}
        route_step = {}
        for row in operations:
            entity_route_id = row['entity_route_id']
            if entity_route_id not in prev_department:
                prev_department[entity_route_id] = None
                route_step[entity_route_id] = 0
            if prev_department[entity_route_id] != departments_dict[row['department_id']]['identity']:
                route_step[entity_route_id] += 1
                prev_department[entity_route_id] = departments_dict[row['department_id']]['identity']
            i = route_step[entity_route_id]
            if '№' in row['identity'] or '-' in row['identity']:
                name = row['identity'][13:18]
            else:
                name = entity_routes_dict[entity_route_id]['identity'][19:]

            if name == '':
                name = row['identity'][13:18]

            row = {
                'identity': '{}_{}'.format(
                    row['identity'][:18],
                    entity_routes_dict[row['entity_route_id']]['identity'][19:]
                ).replace('№', '-'),
                'name': name,
                'incomingDepartmentIdentity': dept_route[entity_route_id][i-1],
                'processingDepartmentIdentity': dept_route[entity_route_id][i],
                'outgoingDepartmentIdentity': dept_route[entity_route_id][i+1],
                'assemblyElementIdentity': entity_dict[
                    entity_routes_dict[
                        row['entity_route_id']
                    ]['entity_id']
                ]['identity']
            }
            if row not in report:
                report.append(row)

        return report

    def export_ca_phases(self):

        self._perform_login()
        entity_routes_dict = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )
        operations = sorted(
            self._get_from_rest_collection('operation'),
            key=lambda k: k['nop']
        )
        departments_dict = list_to_dict(
            self._get_from_rest_collection('department')
        )

        phase_route = {}
        prev_dept = {}
        route_step = {}
        report = []
        for row in operations:
            entity_route_id = row['entity_route_id']
            if 'н' in row['identity'] or 'с' in row['identity']:
                continue
            if entity_route_id not in prev_dept:
                prev_dept[entity_route_id] = None
                phase_route[entity_route_id] = None
                route_step[entity_route_id] = 0
            phase_identity = '{}_{}'.format(
                    row['identity'][:18],
                    entity_routes_dict[entity_route_id]['identity'][19:]
                ).replace('№', '-')
            dept = departments_dict[row['department_id']]['identity']
            if phase_route[entity_route_id] != phase_identity:
                phase_route[entity_route_id] = phase_identity
                route_step[entity_route_id] += 1

            i = route_step[entity_route_id]

            if '№' in row['identity'] or '-' in row['identity']:
                name = phase_route[entity_route_id][13:18]
            else:
                name = entity_routes_dict[entity_route_id]['identity'][19:]

            if name == '':
                name = phase_route[entity_route_id][i][13:18]

            row = {
                'identity': phase_identity,
                'technologicalProcessIdentity':
                    entity_routes_dict[row['entity_route_id']]['identity'],
                'name': name,
                'priority': i,
                'departmentIdentity': prev_dept[entity_route_id] or dept,
            }

            if row not in report:
                report.append(row)

            prev_dept[entity_route_id] = dept

        return report

    def export_ca_operations(self):

        self._perform_login()
        entity_dict = list_to_dict(
            self._get_from_rest_collection('entity')
        )
        entity_routes_dict = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )
        operations = sorted(
            self._get_from_rest_collection('operation'),
            key=lambda k: k['nop']
        )
        departments_dict = list_to_dict(
            self._get_from_rest_collection('department')
        )
        equipment_class_dict = list_to_dict(
            self._get_from_rest_collection('equipment_class')
        )

        operations_filter = lambda x: ('с' not in x['identity']) and ('н' not in x['identity'])

        phases_dict = {}
        operation_priority = {}
        for row in filter(operations_filter, operations):
            entity_route_id = row['entity_route_id']
            if entity_route_id not in phases_dict:
                phases_dict[entity_route_id] = []
                operation_priority[entity_route_id] = 0

            phase_identity = '{}_{}'.format(
                    row['identity'][:18],
                    entity_routes_dict[entity_route_id]['identity'][19:]
                ).replace('№', '-')

            operation_priority[entity_route_id] += 1
            phases_dict[entity_route_id].append(phase_identity)

        report = []
        operation_priority = {}
        for row in filter(operations_filter, operations):
            entity_route_id = row['entity_route_id']
            if entity_route_id not in operation_priority:
                operation_priority[entity_route_id] = 0
            operation_priority[entity_route_id] += 1

            entity_id = entity_routes_dict[entity_route_id]['entity_id']
            equipment_class_id = row['equipment_class_id']
            department_id = row['department_id']

            try:
                phase_identity = phases_dict[entity_route_id][
                                     operation_priority[entity_route_id]
                                 ]
                operation_identity = '{}_{}'.format(
                    phase_identity,
                    row['nop'].split('_')[1]
                )
            except IndexError:
                phase_identity = phases_dict[entity_route_id][-1]
                operation_identity = '{}_{}'.format(
                    phase_identity,
                    '9999'
                )

            row = {
                'identity': operation_identity,
                'transitionIdentity': phase_identity,
                'assemblyElementIdentity':
                    entity_dict[entity_id]['identity'],
                'departmentIdentity':
                    departments_dict[department_id]['identity'],
                'workCenterIdentity':
                    equipment_class_dict[equipment_class_id]['identity'],
                'technologicalProcessIdentity':
                    entity_routes_dict[row['entity_route_id']]['identity'],
                'number': row['nop'],
                'priority': operation_priority[entity_route_id],
                'name': row['name'],
                'pieceTime': round(row['prod_time']/60/60*10000)/10000
            }
            report.append(row)

        return report

    def export_phases_labor(self):

        self._perform_login()

        entity_routes_dict = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )
        operations = self._get_from_rest_collection('operation')
        operation_professions = self._get_from_rest_collection(
            'operation_profession'
        )

        multiplicator = defaultdict(float)
        for row in operation_professions:
            multiplicator[row['operation_id']] += row['amount']

        labor_report = defaultdict(float)
        for row in operations:
            route_identity = '{}_{}'.format(
                row['identity'][:18],
                entity_routes_dict[row['entity_route_id']]['identity'][19:]
            )
            labor_report[
                route_identity
            ] += row['prod_time'] * multiplicator[row['id']]

        return [
            {
                'identity': route_identity.replace('№', '-'),
                'transitionIdentity': route_identity.replace('№', '-'),
                'date': str(date.today()),
                'totalTime': round(
                    labor_report[route_identity] / 60 / 60,
                    4
                ),
            } for route_identity in labor_report
        ]

    # export_type=0 -- для плана запуска
    # export_type=-1 -- для плана выпуска
    def export_bfg_plan(self, export_type):

        self._perform_login()

        entity_routes_dict = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )
        operations = self._get_from_rest_collection('operation')

        operations_list = defaultdict(list)
        for row in operations:
            route_identity = '{}_{}'.format(
                row['identity'][:18],
                entity_routes_dict[row['entity_route_id']]['identity'][19:]
            )
            operations_list[route_identity].append(row)
        for route in operations_list:
            operations_list[route].sort(
                key=lambda i: i['nop']
            )

        operations = self._perform_get(
            'rest/collection/simulation_operation_task?'
            'order_by=start_time&asc=true&'
            'order_by=id&asc=true&'
            'with=simulation_entity_batch&'
            'with=operation&'
            'with=operation.entity_route&'
            'filter={{ simulation_entity_batch.simulation_session_id eq {} }} '
            'and {{ start_time le 720}}'
            'and {{ type eq 0 }}'.format(self._get_main_session())
        )

        report = defaultdict(lambda: defaultdict(float))
        operation_dict = list_to_dict(operations['operation'])
        entity_routes_dict = list_to_dict(operations['entity_route'])

        for row in operations['simulation_operation_task']:
            route_identity = '{}_{}'.format(
                operation_dict[row['operation_id']]['identity'][:18],
                entity_routes_dict[
                    operation_dict[row['operation_id']]['entity_route_id']
                ]['identity'][19:]
            )
            if row['operation_id'] != operations_list[
                route_identity
            ][export_type]['id']:
                continue
            try:
                task_date = (datetime.strptime(
                    row['start_date'],
                    '%Y-%m-%dT%H:%M:%S.%f%z'
                ) - timedelta(3/24)).replace(tzinfo=None)
            except ValueError:
                task_date = (datetime.strptime(
                    row['start_date'],
                    '%Y-%m-%dT%H:%M:%S%z'
                ) - timedelta(3 / 24)).replace(tzinfo=None)

            task_date = datetime.strftime(task_date, '%Y-%m-%d')

            report[route_identity][task_date] += row['entity_amount'] * (
                (row['stop_labor'] or 1) - (row['start_labor'] or 0)
            )

        if export_type == -1:
            qty_column = 'quantityPlanBFG'
        else:
            qty_column = 'quantityLaunch'

        return [
            {
                'identity': '{}_{}'.format(task_date, route.replace('№', '-')),
                'transitionIdentity': route.replace('№', '-'),
                'date': task_date,
                qty_column: report[route][task_date]
            } for route in report for task_date in report[route]
        ]

    def export_equipment(self):

        self._perform_login()

        equipment = self._get_from_rest_collection('equipment')

        departments_dict = list_to_dict(
            self._get_from_rest_collection('department')
        )
        equipments_class_dict = list_to_dict(
            self._get_from_rest_collection('equipment_class')
        )

        return [
            {
                'ID': row['id'],
                'EQUIPMENT_ID': equipments_class_dict[
                    row['equipment_class_id']
                ]['identity'],
                'DEPT_ID': departments_dict[
                    row['department_id']
                ]['identity'],
                'NAME': '###',
                'MODEL': '###',
                'AMOUNT': 1,
                'UTILIZATION': 1
            } for row in equipment
        ]

    def export_ca_daily_tasks(self):

        self._perform_login()

        entities_dict = list_to_dict(
            self._get_from_rest_collection('entity')
        )

        tasks = self._perform_get(
            'rest/collection/simulation_operation_task?'
            'order_by=start_time&asc=true&'
            'order_by=id&asc=true&'
            'with=simulation_entity_batch&'
            'with=operation&'
            'with=operation.entity_route&'
            'filter={{ simulation_entity_batch.simulation_session_id eq {} }} '
            'and {{ start_time le 168}}'
            'and {{ type eq 0 }}'.format(self._get_main_session())
        )

        operations_filter = lambda x: ('с' not in x['identity']) and ('н' not in x['identity'])

        operation_dict = list_to_dict(tasks['operation'])
        entity_routes_dict = list_to_dict(tasks['entity_route'])
        phases_dict = {}
        operation_priority = {}

        for row in filter(
                operations_filter,
                sorted(tasks['operation'], key=lambda x: x['nop'])
        ):
            entity_route_id = row['entity_route_id']
            if entity_route_id not in phases_dict:
                phases_dict[entity_route_id] = []

            phase_identity = '{}_{}'.format(
                    row['identity'][:18],
                    entity_routes_dict[entity_route_id]['identity'][19:]
                ).replace('№', '-')

            phases_dict[entity_route_id].append(phase_identity)
            operation_priority[row['id']] = len(
                phases_dict[entity_route_id]
            )

        report = defaultdict(
            lambda: defaultdict(
                lambda: defaultdict(int)
            )
        )

        operation_entity_dict = {}
        for row in tasks['simulation_operation_task']:
            entity_route_id = operation_dict[row['operation_id']]['entity_route_id']
            nop = operation_dict[row['operation_id']]['nop']

            if 'н' in operation_dict[row['operation_id']]['identity']:
                continue
            if 'с' in operation_dict[row['operation_id']]['identity']:
                continue

            entity_id = entity_routes_dict[entity_route_id]['entity_id']
            if '135503005001' in entities_dict[entity_id]['identity']:
                a = 1

            try:
                phase_identity = phases_dict[entity_route_id][
                                     operation_priority[row['operation_id']]
                                 ]
                operation_identity = '{}_{}'.format(
                    phase_identity,
                    nop.split('_')[1]
                )
            except IndexError:
                phase_identity = phases_dict[entity_route_id][-1]
                operation_identity = '{}_{}'.format(
                    phase_identity,
                    '9999'
                )
            operation_entity_dict[operation_identity] = entities_dict[entity_id]['identity']

            try:
                task_date = (datetime.strptime(
                    row['start_date'],
                    '%Y-%m-%dT%H:%M:%S.%f%z'
                ) - timedelta(3/24)).replace(tzinfo=None)
            except ValueError:
                task_date = (datetime.strptime(
                    row['start_date'],
                    '%Y-%m-%dT%H:%M:%S%z'
                ) - timedelta(3 / 24)).replace(tzinfo=None)

            if task_date.hour >= 12:
                task_time = '19:00:00'
            else:
                task_time = '07:00:00'
            task_date = datetime.strftime(task_date, '%Y-%m-%d')

            report[operation_identity][task_date][task_time] += floor(
                row['entity_amount'] * (row['stop_labor'] or 1)
            ) - floor(row['entity_amount'] * (row['start_labor'] or 0))

        return [
            {
                'identity': '{}_{}_{}'.format(
                    task_date,
                    task_time,
                    operation.replace('№', '-'),
                ),
                'operationIdentity': operation,
                'assemblyElementIdentity': operation_entity_dict[operation],
                'quantityPlan': report[operation][task_date][task_time],
                'dateBegin': task_date,
                'timeBegin': task_time,
            } for operation in report
            for task_date in report[operation]
            for task_time in report[operation][task_date]
        ]

    def export_plan(self, plan_name):
        self._perform_login()
        plan = self._perform_get(
            'rest/collection/plan?filter=name eq {}'
            '&with=order&with=order.order_entry'.format(plan_name)
        )
        order = list_to_dict(plan['order'])
        order_entry = plan['order_entry']
        entity_dict = list_to_dict(
            self._get_from_rest_collection('entity')
        )

        return [
            {
                'ORDER': '###',
                'IDENTITY': '###',
                'NAME': '###',
                'CODE': entity_dict[row['entity_id']]['identity'],
                'AMOUNT': row['amount'],
                'DATE': order[row['order_id']]['date_from'],
                'NUMBER_OF_BATCHES': 1,
            } for row in order_entry
        ]

    def export_ca_wip(self):

        self._perform_login()

        wip_batches = self._get_from_rest_collection('entity_batch')

        operations = list_to_dict(
            self._get_from_rest_collection('operation')
        )

        entity_routes = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )

        main_routes = {
            entity_routes[entity_route_id]['entity_id']:
                entity_routes[entity_route_id]['identity']
            for entity_route_id in filter(
                lambda x: entity_routes[x]['alternate'] is False, entity_routes
            )
        }

        entities = list_to_dict(
            self._get_from_rest_collection('entity')
        )

        report = []
        for row in wip_batches:
            if row['operation_id'] is None:
                if row['operation_progress'] == 0:
                    continue
                if row['entity_id'] not in main_routes:
                    continue
                transition_identity = '{}_{}'.format(
                    entities[row['entity_id']]['identity'],
                    main_routes[row['entity_id']][19:],
                )
            else:
                transition_identity = '{}_{}'.format(
                    operations[row['operation_id']]['identity'],
                    entity_routes[row['entity_route_id']]['identity'][19:],
                )
            report.append({
                'identity': row['identity'],
                'departmentIdentity': row['identity'][-4:],
                'transitionIdentity': transition_identity,
                'quantity': round(row['amount']),
                'dateTime': datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            })

        return report

    def export_pg_wip(self):

        self._perform_login()

        wip_batches = self._get_from_rest_collection('entity_batch')

        operations = list_to_dict(
            self._get_from_rest_collection('operation')
        )

        entity_routes = list_to_dict(
            self._get_from_rest_collection('entity_route')
        )

        main_routes = {
            entity_routes[entity_route_id]['entity_id']:
                entity_routes[entity_route_id]['identity']
            for entity_route_id in filter(
                lambda x: entity_routes[x]['alternate'] is False, entity_routes
            )
        }

        entities = list_to_dict(
            self._get_from_rest_collection('entity')
        )

        report = []
        for row in wip_batches:
            if row['operation_id'] is None:
                if row['operation_progress'] == 0:
                    continue
                if row['entity_id'] not in main_routes:
                    continue
                transition_identity = '{}_{}'.format(
                    entities[row['entity_id']]['identity'],
                    main_routes[row['entity_id']][19:],
                )
            else:
                transition_identity = '{}_{}'.format(
                    operations[row['operation_id']]['identity'],
                    entity_routes[row['entity_route_id']]['identity'][19:],
                )
            report.append({
                'identity': row['identity'],
                'warehouse': '5794',
                'transitionIdentity': transition_identity,
                'quantity': round(row['amount']),
                'date': datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            })

        return report

    @classmethod
    def from_config(cls, config):
        return cls(
            config['login'],
            config['password'],
            config['url'],
        )
