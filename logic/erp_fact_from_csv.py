import csv


def get_erp_fact_from_csv(csv_file):
    with open(csv_file, 'r') as input_file:
        data = csv.DictReader(input_file)
        report = [{
            'identity': '{}_{}_{}'.format(
                row['CODE'],
                row['DATE'],
                row['DEPT_ID_WAREHOUSE']
            ),
            'transitionIdentity': '{}_'.format(row['CODE']),
            'date': row['DATE'].replace('.', '-'),
            'quantityActual': int(row['AMOUNT']),
        } for row in data]
    return report

